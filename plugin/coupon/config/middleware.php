<?php

use app\middleware\AccessControl;
return [
    ''=>[
        \app\middleware\CorsControl::class
    ],
    'admin' => [
        AccessControl::class,
    ]
];
