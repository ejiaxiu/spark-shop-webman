<?php

use support\view\Raw;
use support\view\Twig;
use support\view\Blade;
use support\view\ThinkPHP;

return [
    'handler' => ThinkPHP::class,
    'options' => [
        // 模板目录名
        'view_dir_name' => 'view/default',
        'view_suffix' => 'html',
        'tpl_begin' => '{',
        'tpl_end' => '}',
        'tpl_replace_string' => [
            '{__STATIC__}' => '/static/admin/' . 'default',
            '{__CSS__}' => '/static/admin/' . 'default' . '/css',
            '{__JS__}' => '/static/admin/' . 'default' . '/js',
            '{__IMG__}' => '/static/admin/' . 'default' . '/image',
        ],
    ]
];
