<?php

namespace plugin\coupon\app\admin\service;

use plugin\coupon\app\model\CouponReceiveLog;

class CouponReceiveService
{
    public $receiveCouponStatus = [
        1 => '未使用',
        2 => '已使用',
        3 => '已过期'
    ];
    /**
     * 获取优惠券领取日志
     * @param $param
     * @return array
     */
    public function getCouponReceiveLog($param)
    {
        $limit = $param['limit'];
        $status = $param['status'];
        $userName = $param['user_name'];

        $where = [];
        if (!empty($status)) {
            $where[] = ['status', '=', $status];
        }

        if (!empty($userName)) {
            $where[] = ['user_name', 'like', '%' . $userName . '%'];
        }



        $couponModel = new CouponReceiveLog();
        $list = $couponModel->with('coupon')->where($where)->orderBy('id', 'desc')->paginate($limit);
        $list=returnPage($list);
        foreach ($list['data'] as $k=>$item){
            $list['data'][$k]['status_txt'] =$this->receiveCouponStatus[$item['status']];
        }

        return dataReturn(0, 'success', $list);
    }

}