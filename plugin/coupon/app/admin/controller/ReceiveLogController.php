<?php

namespace plugin\coupon\app\admin\controller;

use app\admin\controller\Curd;
use plugin\coupon\app\admin\service\CouponReceiveService;
use support\Request;

class ReceiveLogController extends Curd
{
    /**
     * 使用列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $couponService = new CouponReceiveService();
            $res = $couponService->getCouponReceiveLog($request->all());
            return $this->success($res);
        }

        return view('receive_log/index');
    }
}