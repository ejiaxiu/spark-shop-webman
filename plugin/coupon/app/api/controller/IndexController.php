<?php

namespace plugin\coupon\app\api\controller;

use plugin\coupon\app\api\service\CouponService;

class IndexController
{
    /**
     * 获取可领取的优惠券
     */
    public function getCouponList()
    {
        $param = request()->all();

        $couponService = new CouponService();
        return json($couponService->getCanReceiveList($param));
    }
}
