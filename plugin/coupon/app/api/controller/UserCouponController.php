<?php

namespace plugin\coupon\app\api\controller;

use plugin\coupon\app\api\service\CouponReceiveService;
use plugin\coupon\app\api\service\CouponService;

class UserCouponController
{
    /**
     * 获取可用的优惠券
     */
    public function getMyValid()
    {
        $param = request()->post();

        $couponReceiveService = new CouponReceiveService();
        return json($couponReceiveService->getMyCoupon($param));
    }

    /**
     * 领取优惠券
     */
    public function receive()
    {
        $param =  request()->post();

        $couponReceiveService = new CouponReceiveService();
        return json($couponReceiveService->userReceive($param));
    }

    /**
     * 获取我的优惠券
     */
    public function myCoupon()
    {
        $param = request()->all();

        $couponService = new CouponService();
        return json($couponService->getMyCouponList($param));
    }
}
