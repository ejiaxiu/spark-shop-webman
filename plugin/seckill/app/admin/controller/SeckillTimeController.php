<?php

namespace plugin\seckill\app\admin\controller;

use app\admin\controller\Curd;
use plugin\seckill\app\admin\service\SeckillTimeService;
use support\Request;

class SeckillTimeController extends Curd
{

    /**
     * 秒杀时间段
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $seckillTimeService = new SeckillTimeService();
            $res = $seckillTimeService->getList($request->all());
            return $this->success($res);
        }

        return view('seckill_time/index');
    }

    /**
     * 添加时间段
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $seckillTimeService = new SeckillTimeService();
            $res = $seckillTimeService->addTime($param);
            return $this->success($res);
        }

        return view('seckill_time/add');
    }

    /**
     * 编辑时间段
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $seckillTimeService = new SeckillTimeService();
            $res = $seckillTimeService->editTime($param);
            return $this->success($res);
        }

        return view('seckill_time/edit');
    }

    /**
     * 删除时间段
     */
    public function del(Request $request)
    {
        if (request()->isAjax()) {

            $id = $request->input('id');

            $seckillTimeService = new SeckillTimeService();
            $res = $seckillTimeService->delTime($id);
            return $this->success($res);
        }
    }
}