<?php

namespace plugin\seckill\app\admin\controller;

use app\admin\controller\Curd;
use plugin\seckill\app\admin\service\SeckillService;
use plugin\seckill\app\model\SeckillTime;
use support\Request;

class SeckillController extends Curd
{

    /**
     * 秒杀列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->all();

            $seckillService = new SeckillService();
            $res = $seckillService->getList($param);
            return $this->success($res);
        }

        return view('seckill/index');
    }

    /**
     * 添加秒杀商品
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $seckillService = new SeckillService();
            $res = $seckillService->addSeckillGoods($param);
            return $this->success($res);
        }

        $seckillTimeModel = new SeckillTime();
        $list = $seckillTimeModel->getAllList([
            'status' => 1
        ], ['*'], ['sort'=> 'desc']);

        return $this->json(0, 'success', [
            'seckill_time' => toArray($list)
        ]);
    }

    /**
     * 编辑秒杀商品
     */
    public function edit(Request $request)
    {
        $seckillService = new SeckillService();

        if ($request->method() === 'POST') {

            $param = $request->post();

            $res = $seckillService->editSeckillGoods($param);
            return $this->success($res);
        }

        return $this->success($seckillService->buildEditParam($request->input('activity_id')));
    }

    /**
     * 删除秒杀商品
     */
    public function del(Request $request)
    {
        if (request()->isAjax()) {

            $id = $request->input('id');

            $seckillService = new SeckillService();
            $res = $seckillService->delSeckill($id);
            return json($res);
        }
    }
}