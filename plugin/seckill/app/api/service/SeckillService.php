<?php

namespace plugin\seckill\app\api\service;

use plugin\seckill\app\model\SeckillActivity;

class SeckillService
{

    /**
     * 秒杀信息列表
     * @param $param
     * @return array
     */
    public function getSeckillList($param)
    {
        $limit = $param['limit'];
        $checkTime = $param['time'];

        $seckillTimeModel = new SeckillTimeService();
        $timeList = $seckillTimeModel->getTimeList()['data']->toArray();

        $nowHour = empty($checkTime) ? date('H') : intval(date('H', strtotime($checkTime)));
        $seckillHour = 0;
        $seckillTimeId = 0;
        $active = 0;
        $endHour = 0;
        $timeLine = [];
        // 限制显示4条
        $offsetLimit = 4;
        $realHour = date('H');

        // 按限制条数整理数据
        $finalTimeLine = [];
        $slices = ceil(count($timeList) / $offsetLimit);
        for ($i = 0; $i < $slices; $i++) {
            $finalTimeLine[] = array_slice($timeList, $i * $offsetLimit, $offsetLimit);
        }

        $hasMatch = false;
        foreach ($finalTimeLine as $v) {

            foreach ($v as $key => $vo) {

                if ($nowHour >= $vo['start_hour'] && $nowHour < ($vo['start_hour'] + $vo['continue_hour'])) {
                    $seckillTimeId = $vo['id'];
                    $seckillHour = $vo['start_hour'];

                    $active = $key;
                    $endHour = $vo['start_hour'] + $vo['continue_hour'];

                    foreach ($v as $time => $val) {
                        $realHour = intval($realHour);
                        $runTime = $val['start_hour'] + $val['continue_hour'];
                        $hour = $val['start_hour'] < 10 ? '0' . $val['start_hour'] : $val['start_hour'];
                        $hour .= ':00';
                        $timeLine[$time]['start_hour'] = $hour;

                        if ($realHour >= $val['start_hour'] && $realHour < $runTime) {
                            $timeLine[$time]['status'] = 2; // 进行中
                        } else if ($realHour >= $runTime) {
                            $timeLine[$time]['status'] = 3; // 已结束
                        } else {
                            $timeLine[$time]['status'] = 1; // 未开始
                        }
                    }
                    $hasMatch = true;
                    break;
                }
            }

            if ($hasMatch) {
                break;
            }
        }

        $seckillModel = new SeckillActivity();
        $list = $seckillModel->select(['id', 'goods_id', 'goods_rule', 'pic','name', 'original_price', 'seckill_price', 'stock', 'sales','status'])
            ->where('status', 2)->where('is_open', 1)
            ->where('seckill_time_id', $seckillTimeId)
            ->where('start_time', '<', now())
            ->where('end_time', '>', now())
            ->paginate($limit);
        return dataReturn(0, 'success', [
            'start_hour' => $seckillHour,
            'active' => $active,
            'endHour' => date('Y-m-d ') . $endHour . ':00:00',
            'list' => $list,
            'time_line' => $timeLine
        ]);
    }
}
