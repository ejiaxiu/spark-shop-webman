<?php

namespace plugin\seckill\app\api\service;

use plugin\seckill\app\model\SeckillTime;

class SeckillTimeService
{
    /**
     * 秒杀时间段
     * @return array
     */
    public function getTimeList()
    {
        $seckillTimeModel = new SeckillTime();
        // 缓存2小时
        $list = $seckillTimeModel->where('status', 1)->orderBy('sort','desc')->get();

        return dataReturn(0, 'success', $list);
    }
}
