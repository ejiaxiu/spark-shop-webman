<?php

namespace plugin\seckill\app\api\controller;

use plugin\seckill\app\api\service\SeckillService;

class IndexController
{
    /**
     * 秒杀商品列表
     */
    public function index()
    {
        $param = request()->all();

        $seckillService = new SeckillService();
        return json($seckillService->getSeckillList($param));
    }

    /**
     * 秒杀商品详情
     */
    public function detail()
    {
        $seckillId = input('param.seckill_id');

        $seckillService = new SeckillService();
        return json($seckillService->seckillDetail($seckillId));
    }

    /**
     * 秒杀订单商品信息
     */
    public function goodsInfo()
    {
        $param = input('post.');

        $orderService = new OrderService();
        return json($orderService->seckillGoodsInfo($param));
    }

    /**
     * 订单试算
     */
    public function trail()
    {
        $param = input('post.');

        $orderService = new OrderService();
        return json($orderService->trail($param, 2));
    }

    /**
     * 创建订单
     */
    public function createOrder()
    {
        $param = input('post.');
        $param['platform'] = '';

        $orderService = new OrderService();
        return json($orderService->createorder($param, 2));
    }
}
