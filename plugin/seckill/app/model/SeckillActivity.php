<?php

namespace plugin\seckill\app\model;

use app\model\Base;

class SeckillActivity extends Base
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seckill_activity';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 活动商品
     */
    public function activityGoods()
    {
        return $this->hasMany(SeckillActivityGoods::class, 'activity_id', 'id');
    }
}