<?php

namespace plugin\seckill\event;

use plugin\seckill\app\api\service\SeckillTimeService;
use plugin\seckill\app\model\SeckillActivity;

class HomeData
{

    /**
     *  秒杀首页信息
     * @param $param
     * @return array
     */
    public function handle($param)
    {
        try {
            $seckillTimeService = new SeckillTimeService();
            $timeList = $seckillTimeService->getTimeList()['data'];
            $timeList = toArray($timeList);
            $nowHour = date('H');
            $seckillHour = 0;
            $seckillTimeId = 0;
            $continueHour = 0;

            foreach ($timeList as $vo) {

                $datetimeArr = [];
                for ($i = 0; $i < $vo['continue_hour']; $i++) {
                    $datetimeArr[] = $vo['start_hour'] + $i;
                }

                if (in_array($nowHour, $datetimeArr)) {
                    $seckillTimeId = $vo['id'];
                    $seckillHour = $vo['start_hour'];
                    $continueHour = $vo['continue_hour'];
                    break;
                }
            }

            $seckillModel = new SeckillActivity();
            $list = $seckillModel->select(['id', 'goods_id', 'goods_rule', 'pic','name', 'original_price', 'seckill_price', 'stock', 'sales'])
                ->where('status', 2)->where('is_open', 1)
                ->where('seckill_time_id', $seckillTimeId)
                ->where('start_time', '<', now())
                ->where('end_time', '>', now())
                ->limit(3)->get();
            $list=toArray($list);
            return dataReturn(0, 'success', compact('list', 'seckillHour', 'continueHour'));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            return [];
        }

    }

}
