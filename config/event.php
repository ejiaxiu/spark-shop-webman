<?php

return [
    'plugin.seckill.seckillHomeData' => [
        [\plugin\seckill\event\HomeData::class, 'handle'],
        // ...其它事件处理函数...
    ],
];
