<?php

namespace app\admin\service;

use app\model\order\OrderComment;

class AppraiseService
{


    /**
     * 获取评价列表
     * @param $param
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function getList($param)
    {
        $limit = $param['limit'];
        $type = $param['type'];
        $appraiseTime = $param['create_time'];

        $where = [];
        if (!empty($type)) {
            $where[] = ['type', '=', $type];
        }

        if (!empty($appraiseTime)) {
            $where[] = ['create_time', 'between', [$appraiseTime[0] . ' 00:00:00', $appraiseTime[1] . ' 23:59:59']];
        }

        $appraise = config('shop.appraise');
        $commentModel = new OrderComment();
        $list = $commentModel->where($where)->orderBy('id','desc')->paginate($limit);
        $list=returnPage($list);
        foreach ($list['data'] as $key=>$item){
            $list['data'][$key]['type']=$appraise[$item['type']];
        }

        return dataReturn(0, 'success', $list);
    }
}