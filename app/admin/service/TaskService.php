<?php

namespace app\admin\service;

use app\model\system\CrontabTask;
use app\model\system\CrontabTaskLog;
use think\Validate;

class TaskService
{


    public function getList($param)
    {
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $taskModel = new CrontabTask();
        $where = [];
        $list = $taskModel->where($where)->orderBy('id', 'desc')->paginate($limit);
        return dataReturn(0, 'success', $list);
    }

    /**
     * 添加定时任务
     * @param $param
     * @return array
     */
    public function addTask($param)
    {
        $setExpressModel = new CrontabTask();
        $has = $setExpressModel->checkUnique([
            'title' => $param['title']
        ])['data'];

        if (!empty($has)) {
            return dataReturn(-2, '定时任务名称已存在');
        }
        $param['create_time'] = time();
        return $setExpressModel->insertOne($param);
    }
    public function getFlowList($param)
    {
        $limit = isset($param['limit']) ? $param['limit'] : 10;

        $taskModel = new CrontabTaskLog();
        $where = ['sid' => $param['sid']];
        $list = $taskModel->where($where)->orderBy('id', 'desc')->paginate($limit);
        return dataReturn(0, 'success', $list);
    }
}