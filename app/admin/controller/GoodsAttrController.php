<?php

namespace app\admin\controller;

use app\admin\service\GoodsAttrService;
use app\model\goods\GoodsAttrTpl;
use support\Request;
use support\View;

class GoodsAttrController extends Curd
{


    public  function  index(Request $request){

        if (request()->isAjax()) {

            $goodsAttrService = new GoodsAttrService();
            $res = $goodsAttrService->getList($request->all());
            return $this->success($res);
        }
        return view('goods_attr/index');
    }

    /**
     * 添加
     */
    public function add( Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();

            $goodsAttrService = new GoodsAttrService();
            $res = $goodsAttrService->addGoodsAttr($param);
            return $this->success($res);
        }

        return view('goods_attr/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();

            $goodsAttrService = new GoodsAttrService();
            $res = $goodsAttrService->editGoodsAttr($param);
            return json($res);
        }

        $id = $request->input('id');
        $goodsAttrTplModel = new GoodsAttrTpl();
        $info = $goodsAttrTplModel->findOne([
            'id' => $id
        ])['data'];

        View::assign([
            'info' => $info,
            'attr' => json_decode($info['value'], true)
        ]);

        return view('goods_attr/edit');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');

        $goodsAttrTplModel = new GoodsAttrTpl();
        $info = $goodsAttrTplModel->delById($id);

        return $this->success($info);
    }

}