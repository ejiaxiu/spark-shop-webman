<?php

namespace app\admin\controller;

use app\admin\service\SliderService;
use app\model\system\WebsiteSlider;
use support\Request;
use support\View;

class SliderController extends Curd
{


    public  function  index(Request $request){
        if (request()->isAjax()) {

            $sliderService = new SliderService();
            $list = $sliderService->getList($request->all());
            return $this->success($list);
        }
        return view('slider/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $sliderService = new SliderService();
            $res = $sliderService->addSlider($param);
            return $this->success($res);
        }

        return view('slider/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $sliderService = new SliderService();
            $res = $sliderService->editSlider($param);
            return json($res);
        }

        $id = $request->input('id');
        $websiteSliderModel = new WebsiteSlider();
        View::assign([
            'info' => $websiteSliderModel->findOne([
                'id' => $id
            ])['data']
        ]);

        return view('slider/edit');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');

        $websiteSliderModel = new WebsiteSlider();
        $info = $websiteSliderModel->delById($id);
        return $this->success($info);
    }
}