<?php

namespace app\admin\controller;

use app\admin\service\LabelService;
use app\model\user\UserLabel;
use support\Request;
use support\View;

class LabelController extends Curd
{
    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $labelService = new LabelService();
            $res = $labelService->getList($request->all());
            return $this->success($res);
        }
        return view('label/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $labelService = new LabelService();
            $res = $labelService->addLabel($param);
            return $this->success($res);
        }

        return view('label/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $labelService = new LabelService();
            $res = $labelService->editLabel($param);
            return $this->success($res);
        }

        $id = $request->input('id');
        $labelModel = new UserLabel();
        View::assign([
            'info' => $labelModel->findOne([
                'id' => $id
            ])['data']
        ]);

        return view('label/edit');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');

        $labelModel = new UserLabel();
        $info = $labelModel->delById($id);

        return $this->success($info);
    }
}