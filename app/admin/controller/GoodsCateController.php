<?php

namespace app\admin\controller;

use app\admin\service\GoodsCateService;
use app\model\goods\GoodsCate;
use support\Request;
use support\View;

class GoodsCateController extends Curd
{

    public function index(Request $request)
    {

        if (request()->isAjax()) {

            $goodsCate = new GoodsCate();
            $list = $goodsCate->getAllList([], ["*"], ["id" => "asc"]);

            return $this->json(0, 'success', makeTree($list->toArray()));
        }
        return view('goods_cate/index');
    }

    public function add(Request $request)
    {
        if (request()->isAjax()) {
            $param = $request->post();
            $goodsCateService = new GoodsCateService();
            $res = $goodsCateService->addGoodsCate($param);
            return $this->json(0, 'success', $res);
        }

        View::assign([
            'pid' => $request->input('pid', 0),
            'pName' => $request->input('pname', ''),
        ]);
        return view("goods_cate/add");
    }

    public function edit(Request $request)
    {
        if (request()->isAjax()) {
            $param = $request->post();
            $goodsCateService = new GoodsCateService();
            $res = $goodsCateService->editGoodsCate($param);
            return $this->json(0, 'success', $res);
        }

        $pid = $request->input('pid');
        $id = $request->input('id');
        $goodsCateModel = new GoodsCate();
        if (0 == $pid) {
            $pName = '顶级分类';
        } else {
            $pName = $goodsCateModel->getInfoById($pid)['data']['name'];
        }
        View::assign([
            'info' => json_encode($goodsCateModel->findOne([
                'id' => $id
            ])['data']),
            'pid' => $pid,
            'pName' => $pName,
        ]);
        return view("goods_cate/edit");
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');

        $goodsCateService = new GoodsCateService();
        $res = $goodsCateService->delGoodsCate($id);
        return $this->success($res);
    }
}