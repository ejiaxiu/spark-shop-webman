<?php

namespace app\admin\controller;

use app\admin\service\ShippingService;
use app\model\system\ShippingTemplates;
use app\model\system\ShippingTemplatesRegion;
use support\Request;
use support\View;

class ShippingController extends Curd
{
    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $shippingService = new ShippingService();
            $res = $shippingService->getList($request->all());
            return $this->success($res);
        }

        return view('shipping/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if (request()->isAjax()) {
            $param = $request->post();
            $shippingService = new ShippingService();
            $res = $shippingService->addShipping($param);
            return $this->success($res);
        }

        return view('shipping/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();

            $shippingService = new ShippingService();
            $res = $shippingService->editShipping($param);
            return $this->success($res);
        }

        $id = $request->input('id');
        $shippingTemplatesModel = new ShippingTemplates();
        $shippingTemplatesRegionModel = new ShippingTemplatesRegion();
        $extend = $shippingTemplatesRegionModel->with(['province', 'city'])->where('tpl_id', $id)->get()->toArray();

        $country = [];
        foreach ($extend as $key => $vo) {
            if ($vo['province_id'] == 0 && $vo['city_id'] == 0) {
                $country = $vo;
                unset($extend[$key]);
                break;
            }
        }

        // 整理模板数据
        $extendMap = [];
        foreach ($extend as $vo) {
            $extendMap[$vo['uniqid']][] = $vo;
        }

        $shippingService = new ShippingService();
        View::assign([
            'info' => json_encode($shippingTemplatesModel->findOne([
                'id' => $id
            ])['data']),
            'extend' => json_encode($shippingService->formatShowParam($extendMap)),
            'country' => json_encode($country)
        ]);

        return view('shipping/edit');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        $shippingTemplatesModel = new ShippingTemplates();
        $info = $shippingTemplatesModel->updateById([
            'is_del' => 2
        ], $id);
        $info['msg'] = '删除成功';
        return $this->success($info);
    }
}