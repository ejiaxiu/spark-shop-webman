<?php

namespace app\admin\controller;

use app\admin\service\HomeService;
use app\admin\service\MenuService;
use app\model\system\AdminNode;
use support\Request;
use support\View;

class IndexController extends Curd
{
    public function index(Request $request)
    {
        $this->model=new AdminNode();
        if(admin_id() !=1){
            $authMenu=[];
        }else{
            $authMenu = makeTree((new MenuService())->getSuperAdminNode());
        }
        View::assign([
            'menu' => json_encode($authMenu)
        ]);
        return view('index/index');
    }


    public function  home(Request  $request){

        if ($request->isAjax()) {
            $homeService = new HomeService();
            // 销售数据
            $sellData = $homeService->sellAmountData()['data'];
            // 访问数据
            $pvData = $homeService->userPVData()['data'];
            // 订单量
            $orderData = $homeService->orderData()['data'];
            // 用户注册数据
            $userData = $homeService->userData()['data'];
            // 订单统计图
            $orderChartsData = $homeService->fifteenDayOrderData()['data'];
            // 注册统计图
            $regData = $homeService->registerData()['data'];

            return $this->json(0, 'success', compact('sellData', 'pvData', 'orderData', 'userData', 'orderChartsData', 'regData'));
        }
        return view('index/home');
    }







    

}
