<?php

namespace app\admin\controller;

use app\admin\service\MenuService;
use support\Request;

class MenuController extends Curd
{
    /**
     * 菜单列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {
            $menuService = new MenuService();
            return $this->success($menuService->getMenuTree());
        }
        return view('menu/index');
    }

    /**
     * 新增菜单
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $menuService = new MenuService();
            return $this->success($menuService->addMenu($request->post()));
        }
    }

    /**
     * 编辑菜单
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $menuService = new MenuService();
            return $this->success($menuService->editMenu($request->post()));
        }
    }

    /**
     * 删除菜单
     */
    public function del(Request $request)
    {
        $menuService = new MenuService();
        return $this->success($menuService->delMenu($request->input('id')));
    }

}