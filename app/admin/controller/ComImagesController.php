<?php

namespace app\admin\controller;

use app\admin\service\ComImagesService;
use app\model\system\ComImages;
use support\Request;
use support\View;

class ComImagesController extends Curd
{


    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if ($request->isAjax()) {

            $param = $request->all();

            $comImagesService = new ComImagesService();
            $res = $comImagesService->getList($param);
            return $this->json(0,'success',$res);
        }

        return view('com_images/index');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        set_time_limit(0);
        $ids = $request->input('ids');

        $comImagesService = new ComImagesService();
        $res = $comImagesService->delComImages($ids);
        return $this->success($res);
    }

    /**
     * 移动图片分类
     */
    public function edit(Request $request)
    {
        $ids = array_unique($request->input('ids'));
        $cateId=$request->input('cate_id');
        $comImagesModel = new ComImages();
        $res = $comImagesModel->updateByIds([
            'cate_id' => $cateId
        ], $ids);
        return $this->success($res);

    }

    /**
     * 显示图片选择器
     */
    public function show(Request $request)
    {
        View::assign([
            'type' => $request->input('type', 'img'),
            'img_ext' => config('images.ext'),
            'video_ext' => config('images.video_ext'),
            'limit' => $request->input('limit'),
            'callback' => $request->input('callback')
        ]);

        return view('com_images/show');
    }
}