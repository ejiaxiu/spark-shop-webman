<?php

namespace app\admin\controller;

use app\admin\service\RefundService;
use support\Request;

class RefundController extends Base
{

    /**
     * 退款列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {
            $refundService = new RefundService();
            $res = $refundService->getList($request->all());
            return $this->success($res);
        }

        return view('refund/index');
    }

    /**
     * 订单详情
     */
    public function detail(Request $request)
    {
        $orderId = input('param.id');
        $refundId = input('param.refund');

        $refundService = new RefundService();
        $res = $refundService->getDetail($orderId, $refundId);
        return json($res);
    }

    /**
     * 退货审核
     */
    public function checkRefundGoods(Request $request)
    {
        $param = input('post.');

        $refundService = new RefundService();
        $refundService->checkRefundGoods($param);
        return jsonReturn(0, '操作成功');
    }

    /**
     * 退款审核
     */
    public function checkRefundMoney(Request $request)
    {
        $param = input('post.');

        $refundService = new RefundService();
        $res = $refundService->checkRefundMoney($param);
        return json($res);
    }
}