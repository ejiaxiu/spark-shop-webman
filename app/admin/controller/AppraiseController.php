<?php

namespace app\admin\controller;

use app\admin\service\AppraiseService;
use app\model\order\OrderComment;
use support\Request;

class AppraiseController extends Curd
{
    /**
     * 评价列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $appraiseService = new AppraiseService();
            $res = $appraiseService->getList($request->all());
            return $this->success($res);
        }

        return view('appraise/index');
    }

    /**
     * 删除评价
     */
    public function del(Request $request)
    {
        if (request()->isAjax()) {
            $id = $request->input('id');
            $commentModel = new OrderComment();
            $res = $commentModel->delById($id);
            return $this->success($res);
        }
    }
}