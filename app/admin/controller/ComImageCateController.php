<?php

namespace app\admin\controller;

use app\model\system\ComImagesCate;
use support\Request;

class ComImageCateController extends Curd
{

    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if ($request->isAjax()) {

            $comImagesCateModel = new ComImagesCate();
            $list = $comImagesCateModel->getAllList();

            return $this->json(0, 'success', makeTree($list->toArray()));
        }
    }
}