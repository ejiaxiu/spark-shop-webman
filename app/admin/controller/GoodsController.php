<?php

namespace app\admin\controller;

use app\admin\service\GoodsService;
use app\model\goods\Goods;
use app\model\goods\GoodsCate;
use app\model\goods\GoodsContent;
use app\model\goods\GoodsRuleExtend;
use support\Request;
use support\View;

class GoodsController extends Curd
{

    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $goodsService = new GoodsService();
            $res = $goodsService->getList($request->all());
            return $this->json(0, 'success', toArray($res['data']));
        }
        $goodsCateModel = new GoodsCate();
        $cate = $goodsCateModel->getAllList([
            'status' => 1
        ], ['id', 'pid', 'name'])->toArray();
        View::assign([
            'cate' => json_encode(makeTree($cate))
        ]);
        return view('goods/index');
    }

    public function add(Request $request)
    {

        if (request()->isAjax()) {
            $param = $request->post();
            $goodsService = new GoodsService();
            $res = $goodsService->addGoods($param, 'add');
            return $this->success($res);
        }
        $goodsService = new GoodsService();
        View::assign($goodsService->getBaseParam());
        return view('goods/add');
    }

    public function edit(Request $request)
    {

        if (request()->isAjax()) {

            $param = $request->post();

            $goodsService = new GoodsService();
            $res = $goodsService->editGoods($param, 'edit');
            return $this->success($res);
        }
        $id = $request->input('id');
        $goodsModel = new Goods();
        $goodsRuleModel = new \app\model\goods\GoodsRule();
        $goodsRuleExtendModel = new GoodsRuleExtend();
        $goodsContentModel = new GoodsContent();
        $goodsAttrModel = new \app\model\goods\GoodsAttr();
        View::assign([
            'info' => json_encode($goodsModel->findOne([
                'id' => $id
            ])['data']),
            'ruleData' => json_encode($goodsRuleModel->findOne([
                'goods_id' => $id
            ])['data']),
            'extend' => json_encode($goodsRuleExtendModel->getAllList([
                'goods_id' => $id
            ], ['*'], ['id'=> 'asc'])),
            'content' => json_encode($goodsContentModel->findOne([
                'goods_id' => $id
            ])['data']),
            'attrData' => json_encode($goodsAttrModel->getAllList([
                'goods_id' => $id
            ], ['*'], ['id'=> 'asc']))
        ]);
        $goodsService = new GoodsService();

        View::assign($goodsService->getBaseParam());
        return view('goods/edit');
    }

    /**
     * 上下架
     */
    public function shelf(Request $request)
    {
        $param = $request->post();
        $goodsModel = new Goods();

        $res = $goodsModel->updateByIds([
            'is_show' => $param['is_show']
        ], $param['ids']);

        $res['msg'] = '操作成功';
        return $this->success($res);
    }

    /**
     * 删除商品
     */
    public function del(Request $request)
    {
        $goodsService = new GoodsService();
        return $this->success($goodsService->delGoods($request->input('id')));
    }

    public function recover(Request $request)
    {
        $id = $request->input('id');
        $goodsModel = new Goods();

        $res = $goodsModel->updateByIds([
            'is_del' => 2
        ], $id);

        return $this->success($res);
    }

}
