<?php

namespace app\admin\controller;

use app\admin\service\UserGroupService;
use app\model\user\UserGroup;
use support\Request;
use support\View;

class UserGroupController extends Curd
{
    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $userGroupService = new UserGroupService();
            $res = $userGroupService->getList($request->all());
            return $this->success($res);
        }

        return view('user_group/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();
            $userGroupService = new UserGroupService();
            $res = $userGroupService->addUserGroup($param);
            return $this->success($res);
        }

        return view('user_group/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $userGroupService = new UserGroupService();
            $res = $userGroupService->editUserGroup($param);
            return $this->success($res);
        }

        $id = $request->input('id');
        $userGroupModel = new UserGroup();
        View::assign([
            'info' => $userGroupModel->findOne([
                'id' => $id
            ])['data']
        ]);

        return view('user_group/edit');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');

        $userGroupModel = new UserGroup();
        $info = $userGroupModel->delById($id);

        return $this->success($info);
    }
}