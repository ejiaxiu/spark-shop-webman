<?php

namespace app\admin\controller;

use app\admin\service\MenuService;
use app\admin\service\RoleService;
use support\Request;

class RoleController extends Curd
{


    public  function  index(Request $request){
        if (request()->isAjax()) {
            $roleService = new RoleService();
            $res = $roleService->getList($request->all());
            return $this->success($res);
        }
        return view('role/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $roleService = new RoleService();
            $res = $roleService->addRole($param);
            return $this->success($res);
        }

        return $this->json(0, 'success', (new MenuService())->getNodeTree());
    }


    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();
            $roleService = new RoleService();
            $res = $roleService->editRole($param);
            return $this->success($res);
        }

        return $this->json(0, 'success', (new MenuService())->getNodeTree());
    }



    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');
        $roleService = new RoleService();
        $res = $roleService->delRole($id);
        return $this->success($res);
    }



}