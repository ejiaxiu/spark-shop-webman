<?php

namespace app\admin\controller;

use app\admin\service\UserService;
use app\model\user\User;
use app\model\user\UserLabel;
use support\Request;
use support\View;

class UserController extends Curd
{

    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {
            $userService = new UserService();
            $res = $userService->getList($request->all());
            return $this->success($res);
        }
        $userService = new UserService();
        View::assign($userService->buildBaseParam());

        $labelModel = new UserLabel();
        $labelList = $labelModel->getAllList([], ['id as value','name'], ['id'=> 'asc']);
        View::assign([
            'label' => json_encode($labelList)
        ]);
        return view('user/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {
            $param = $request->post();
            $userService = new UserService();
            $res = $userService->addUser($param);
            return  $this->success($res);
        }
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {
            $param = $request->post();

            $userService = new UserService();
            $res = $userService->editUser($param);
            return  $this->success($res);
        }

        $id = $request->input('id');
        $userModel = new User();
        View::assign([
            'info' => $userModel->findOne([
                'id' => $id
            ])['data']
        ]);

        return view('user/edit');
    }

    /**
     * 余额编辑
     */
    public function balance(Request $request)
    {
        $param = $request->post();

        $userService = new UserService();
        $res = $userService->changeBalance($param);
        return  $this->success($res);
    }
}