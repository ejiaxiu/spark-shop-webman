<?php

namespace app\admin\controller;

use app\admin\service\UserLevelService;
use app\model\user\UserLevel;
use support\Request;
use support\View;

class UserLevelController extends Curd
{
    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {
            $userLevelService = new UserLevelService();
            $res = $userLevelService->getList($request->all());
            return $this->success($res);
        }
        return view('user_level/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $userLevelService = new UserLevelService();
            $res = $userLevelService->addUserLevel($param);
            return $this->success($res);
        }

        return view('user_level/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $userLevelService = new UserLevelService();
            $res = $userLevelService->editUserLevel($param);
            return $this->success($res);
        }

        $id = $request->input('id');
        $userLevelModel = new UserLevel();
        View::assign([
            'info' => $userLevelModel->findOne([
                'id' => $id
            ])['data']
        ]);

        return view('user_level/edit');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');

        $userLevelModel = new UserLevel();
        $info = $userLevelModel->delById($id);

        return $this->success($info);
    }

}