<?php

namespace app\admin\controller;

use app\admin\service\AdminService;
use app\model\system\AdminRole;
use app\model\system\AdminUser;
use support\Request;

class AdminController extends Curd
{

    public function index(Request $request)
    {
        if (request()->isAjax()) {
            $adminService = new AdminService();
            $res = $adminService->getList($request->all());
            return $this->success($res);
        }

        return view('admin/index');
    }

    /**
     * 添加管理员
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {
            $param = $request->post();
            $adminService = new AdminService();
            $res = $adminService->addAdmin($param);
            return $this->success($res);
        }

        $adminRoleModel = new AdminRole();
        $where[] = ['status', '=', 1];
        $where[] = ['id', '>', 1];
        return $this->json(0, 'ok', $adminRoleModel->getAllList($where)->toArray());
    }

    /**
     * 编辑管理员
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $adminService = new AdminService();
            $res = $adminService->editAdmin($param);
            return $this->success($res);
        }
        $adminRoleModel = new AdminRole();
        $where[] = ['status', '=', 1];
        $where[] = ['id', '>', 1];
        return $this->json(0, 'ok', $adminRoleModel->getAllList($where)->toArray());
    }

    /**
     * 删除管理员
     */
    public function del(Request $request)
    {
        $id = $request->input('param.id');
        if ($id === 1) {
            return $this->json(-1, '超级管理员不可以删除');
        }
        $adminUserModel = new AdminUser();
        $res = $adminUserModel->delById($id);
        return $this->success($res);
    }
}