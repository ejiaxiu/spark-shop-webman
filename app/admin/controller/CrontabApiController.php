<?php

namespace app\admin\controller;

use app\admin\service\TaskService;
use app\model\system\CrontabTask;
use support\Request;
use support\View;

class CrontabApiController extends Curd
{

    protected $host = 'http://127.0.0.1:2345';
    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {
            $param = $request->all();
            $taskService = new TaskService();
            $res = $taskService->getList($param);
            return $this->success($res);
        }
        return view('task/index');
    }

    /**
     * 创建任务
     */
    public function add(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();
            $param['frequency'] = $this->getTimerStr($param['frequency']);
            $param['remark'] = '';
            $expressService = new TaskService();
            $res = $expressService->addTask($param);
            return $this->success($res);
        }
        return view('task/add');
    }

    /**
     * 编辑任务
     */
    public function edit()
    {
//        $param = input('post.');
//
//        $res = curlPost($this->host . '/crontab/modify', $param);
        return json([]);
    }

    /**
     * 删除任务
     */
    public function del(Request $request)
    {

        $id = $request->input('id');

        $setExpressModel = new CrontabTask();
        $info = $setExpressModel->delById($id);
        return $this->success($info);
    }

    /**
     * 重启服务
     */
    public function reload()
    {
//        $param = input('post.');
//
//        $res = curlPost($this->host . '/crontab/reload', $param);
//        return json($res);
        return $this->success([]);
    }

    /**
     * 执行日志
     */
    public function flow(Request $request)
    {
        $param = $request->all();

        $taskService = new TaskService();
        $res = $taskService->getFlowList($param);
        return $this->success($res);
    }

    public function getTimerStr($data): string
    {
        $timeStr = '';
        switch ($data['type']) {
            case 1:
                $timeStr = '*/' . $data['second'] . ' * * * * *';
                break;
            case 2:
                $timeStr = '0 */' . $data['minute'] . ' * * * *';
                break;
            case 3:
                $timeStr = '0 0 */' . $data['hour'] . ' * * *';
                break;
            case 4:
                $timeStr = '0 0 0 */' . $data['day'] . ' * *';
                break;
            case 5:
                $timeStr = $data['second'] . ' ' . $data['minute'] . ' ' . $data['hour'] . ' * * *';
                break;
            case 6:
                $timeStr = $data['second'] . ' ' . $data['minute'] . ' ' . $data['hour'] . ' * * ' . ($data['week'] == 7 ? 0 : $data['week']);
                break;
            case 7:
                $timeStr = $data['second'] . ' ' . $data['minute'] . ' ' . $data['hour'] . ' ' . $data['day'] . ' * *';
                break;
        }
        return $timeStr;
    }
}