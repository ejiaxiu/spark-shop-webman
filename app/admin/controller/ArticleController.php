<?php

namespace app\admin\controller;

use app\admin\service\ArticleService;
use app\model\system\Article;
use support\Request;
use support\View;

class ArticleController extends Curd
{

    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $articleService = new ArticleService();
            $res = $articleService->getList($request->all());
            return $this->success($res);
        }

        return view('article/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();
            $articleService = new ArticleService();
            $res = $articleService->addArticle($param);
            return $this->success($res);
        }

        $articleCateModel = new \app\model\system\ArticleCate();
        View::assign([
            'cate' => json_encode($articleCateModel->getAllList([
                'status' => 1
            ], ['id','name']))
        ]);

        return view('article/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $articleService = new ArticleService();
            $res = $articleService->editArticle($param);
            return $this->success($res);
        }

        $articleCateModel = new \app\model\system\ArticleCate();
        $id = $request->input('id');
        $articleModel = new Article();
        View::assign([
            'info' => json_encode($articleModel->findOne([
                'id' => $id
            ])['data']),
            'cate' => json_encode($articleCateModel->getAllList([
                'status' => 1
            ], ['id','name']))
        ]);

        return view('article/edit');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');

        $articleModel = new Article();
        $info = $articleModel->delById($id);

        return $this->success($info);
    }
}