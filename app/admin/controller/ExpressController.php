<?php

namespace app\admin\controller;

use app\admin\service\ExpressService;
use app\model\system\SetExpress;
use support\Request;
use support\View;

class ExpressController extends Curd
{
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->all();

            $expressService = new ExpressService();
            $res = $expressService->getList($param);
            return $this->success($res);
        }
        return view('express/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if (request()->isAjax()) {
            $param = $request->post();
            $expressService = new ExpressService();
            $res = $expressService->addExpress($param);
            return $this->success($res);
        }
        return view('express/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();
            $expressService = new ExpressService();
            $res = $expressService->editExpress($param);
            return $this->success($res);
        }

        $id = $request->get('id');
        $setExpressModel = new SetExpress();
        View::assign([
            'info' => $setExpressModel->findOne([
                'id' => $id
            ])['data']
        ]);

        return view('express/edit');
    }
    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->get('id');

        $setExpressModel = new SetExpress();
        $info = $setExpressModel->delById($id);
        return $this->success($info);
    }

}