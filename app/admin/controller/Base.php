<?php

namespace app\admin\controller;

use support\View;
use support\Model;
use support\Response;

class Base
{

    /**
     * @var Model
     */
    protected $model = null;
    /**
     * 无需登录及鉴权的方法
     * @var array
     */
    protected $noNeedLogin = [];

    /**
     * 需要登录无需鉴权的方法
     * @var array
     */
    protected $noNeedAuth = [];

    /**
     * 数据限制
     * 例如当$dataLimit='personal'时将只返回当前管理员的数据
     * @var string
     */
    protected $dataLimit = null;

    /**
     * 数据限制字段
     */
    protected $dataLimitField = 'admin_id';

    public function __construct()
    {
        View::assign([
            'nickname' => admin('name'),
            'avatar' => admin('avatar'),
            'version' => '1.0.0',
            'title' => 'SparkShop',
            'is_debug' => '0'
        ]);
    }


    /**
     * 返回格式化json数据
     *
     * @param int $code
     * @param string $msg
     * @param array $data
     * @return Response
     */
    protected function json(int $code, string $msg = 'ok', array $data = []): Response
    {
        return json(['code' => $code, 'data' => $data, 'msg' => $msg]);
    }

    protected function success(array $data)
    {

        if (!isset($data['code'])) {
            $data['code'] = 0;
        }
        if (!isset($data['msg'])) {
            $data['msg'] = 'success';
        }
        if (!isset($data['data'])) {
            $data['data'] = [];
        }
        return json($data);
    }

}