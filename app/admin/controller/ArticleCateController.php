<?php

namespace app\admin\controller;

use app\admin\service\ArticleCateService;
use app\model\system\ArticleCate;
use support\Request;
use support\View;

class ArticleCateController extends Curd
{
    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {
            $articleCateService = new ArticleCateService();
            $res = $articleCateService->getList($request->all());
            return $this->success($res);
        }
        return view('article_cate/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $articleCateService = new ArticleCateService();
            $res = $articleCateService->addArticleCate($param);
            return $this->success($res);
        }

        return view('article_cate/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $articleCateService = new ArticleCateService();
            $res = $articleCateService->editArticle($param);
            return $this->success($res);
        }

        $id = $request->input('id');
        $articleCateModel = new ArticleCate();
        View::assign([
            'info' => $articleCateModel->findOne([
                'id' => $id
            ])['data']
        ]);

        return view('article_cate/edit');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');

        $articleModel = new \app\model\system\Article();
        $has = $articleModel->select(['id'])->where('cate_id', $id)->first();
        if (!empty($has)) {
            return $this->json(-1, "该分类下有文章不可删除");
        }

        $articleCateModel = new ArticleCate();
        $info = $articleCateModel->delById($id);
        return $this->success($info);
    }
}