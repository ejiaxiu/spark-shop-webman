<?php

namespace app\admin\controller;

use app\admin\service\LinksService;
use app\model\system\WebsiteLinks;
use support\Request;
use support\View;

class LinksController extends Curd
{
    /**
     * 获取列表
     */
    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $linksService = new LinksService();
            $res = $linksService->getList($request->all());
            return $this->success($res);
        }
        return view('links/index');
    }

    /**
     * 添加
     */
    public function add(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $linksService = new LinksService();
            $res = $linksService->addLinks($param);
            return $this->success($res);
        }

        return view('links/add');
    }

    /**
     * 编辑
     */
    public function edit(Request $request)
    {
        if ($request->method() === 'POST') {

            $param = $request->post();

            $linksService = new LinksService();
            $res = $linksService->editLinks($param);
            return $this->success($res);
        }

        $id = $request->input('id');
        $websiteLinksModel = new WebsiteLinks();
        View::assign([
            'info' => $websiteLinksModel->findOne([
                'id' => $id
            ])['data']
        ]);

        return view('links/edit');
    }

    /**
     * 删除
     */
    public function del(Request $request)
    {
        $id = $request->input('id');

        $websiteLinksModel = new WebsiteLinks();
        $info = $websiteLinksModel->delById($id);
        return $this->success($info);
    }

}