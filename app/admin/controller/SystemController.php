<?php

namespace app\admin\controller;

use app\admin\service\SystemService;
use app\model\user\UserAgreement;
use support\Request;
use support\View;

class SystemController extends Curd
{

    public function index(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();

            $systemService = new SystemService();
            $systemService->saveSystem($param);
            return $this->json(0, '保存成功');
        }
        View::assign([
            'base' => json_encode(getConfByType('base')),
            'shop_base' => json_encode(getConfByType('shop_base')),
            'shop_user_level' => json_encode(getConfByType('shop_user_level')),
            'shop_refund' => json_encode(getConfByType('shop_refund'))
        ]);
        return view('system/index');
    }

    public function pay(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();
            if (isset($param['file'])) {
                unset($param['file']);
            }
            $systemService = new SystemService();
            $systemService->saveSystem($param);
            return $this->json(0, '保存成功');
        }
        View::assign([
            'alipay' => json_encode(getConfByType('alipay')),
            'wechat' => json_encode(getConfByType('wechat_pay')),
            'balance' => json_encode(getConfByType('balance_pay')),
        ]);
        return view('system/pay');
    }

    public function miniapp(Request $request)
    {

        if (request()->isAjax()) {

            $param = $request->post();

            $systemService = new SystemService();
            $systemService->saveSystem($param);
            return $this->json(0, '保存成功');
        }
        View::assign([
            'info' => json_encode(getConfByType('miniapp')),
        ]);
        return view('system/miniapp');
    }

    /**
     * 物流配置
     */
    public function express(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();

            $systemService = new SystemService();
            $systemService->saveSystem($param);
            return $this->json(0, '保存成功');
        }

        View::assign([
            'info' => json_encode(getConfByType('express'))
        ]);
        return view('system/express');
    }

    /**
     * 短信配置
     */
    public function sms(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();

            $systemService = new SystemService();
            $systemService->saveSystem($param);
            return $this->json(0, '保存成功');
        }

        View::assign([
            'info' => json_encode(getConfByType('sms'))
        ]);

        return view('system/sms');
    }


    /**
     * 三方存储配置
     */
    public function store(Request $request)
    {
        if (request()->isAjax()) {
            $param = $request->post();

            $systemService = new SystemService();
            $systemService->saveSystem($param);
            return $this->json(0, '保存成功');
        }

        View::assign([
            'store' => json_encode(getConfByType('store')),
            'aliyun' => json_encode(getConfByType('store_oss')),
            'qiniu' => json_encode(getConfByType('store_qiniu')),
            'qcloud' => json_encode(getConfByType('store_tencent')),
        ]);

        return view('system/store');
    }

    /**
     * 协议配置
     */
    public function agreement(Request $request)
    {
        if (request()->isAjax()) {

            $param = $request->post();

            $userAgreementModel = new UserAgreement();
            $has = $userAgreementModel->findOne([
                'type' => $param['type']
            ], 'id')['data'];

            if (!empty($has)) {
                $userAgreementModel->updateById([
                    'content' => $param['content'],
                    'update_time' => now()
                ], $has['id']);
            } else {
                $userAgreementModel->insertOne([
                    'type' => $param['type'],
                    'content' => $param['content'],
                    'create_time' => now()
                ]);
            }

            return $this->json(0, '保存成功');
        }

        $userAgreementModel = new UserAgreement();
        $agreementList = $userAgreementModel->getAllList();

        $agreementMap = [
            1 => '',
            2 => ''
        ];

        if (!empty($agreementList)) {
            foreach ($agreementList as $vo) {
                $agreementMap[$vo['type']] = $vo['content'];
            }
        }

        View::assign([
            'agreementMap' => json_encode($agreementMap)
        ]);

        return view('system/agreement');
    }
}