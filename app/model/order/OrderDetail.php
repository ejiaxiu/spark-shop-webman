<?php

namespace app\model\order;

use app\model\Base;

class OrderDetail extends Base
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_detail';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    public function comment()
    {
        return $this->hasOne(OrderComment::class, 'order_detail_id', 'id');
    }
}