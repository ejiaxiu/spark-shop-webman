<?php

namespace app\model\system;

use app\model\Base;

class Article extends Base
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'article';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    public function cateInfo()
    {
        return $this->hasOne(ArticleCate::class, 'id', 'cate_id');
    }
}