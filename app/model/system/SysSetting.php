<?php

namespace app\model\system;

use app\model\Base;

/**
 * @property integer $id (主键)
 * @property string $type 键
 * @property string $text 值
 * @property string $key 键
 * @property string $value 值
 */
class SysSetting extends Base
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sys_setting';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 指示是否自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * 获取打开的支付方式
     * @return array
     */
    public function getOpenWay()
    {
        try {

            $res = $this->select(['value','type'])->whereIn('key', ['wechat_pay_open', 'alipay_open', 'balance_open'])->get();
            $payMap = [];
            if (!empty($res)) {
                foreach ($res as $vo) {
                    $payMap[$vo['type']] = $vo['value'];
                }
            }
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $payMap);
    }
}
