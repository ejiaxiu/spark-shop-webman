<?php

namespace app\model\system;

use app\model\Base;

class ShippingTemplatesRegion extends Base
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shipping_templates_region';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * 指示是否自动维护时间戳
     *
     * @var bool
     */
    public $timestamps = false;


    public function province()
    {
        return $this->hasOne(SetCity::class, 'id', 'province_id');
    }

    public function city()
    {
        return $this->hasOne(SetCity::class, 'id', 'city_id');
    }
}