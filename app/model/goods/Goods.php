<?php

namespace app\model\goods;

use app\model\Base;

class Goods extends Base
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'goods';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public function cate()
    {
        return $this->hasOne(GoodsCate::class, 'id', 'cate_id');
    }
}