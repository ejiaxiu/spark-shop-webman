<?php

namespace app\model;

use DateTimeInterface;
use support\Model;

class Base extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * 格式化日期
     *
     * @param DateTimeInterface $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getAllList($where = [], $field = ["*"], $order = ["id" => "desc"])
    {
        $list = $this->query()->select($field)->where($where);
        if ($order) {
            foreach ($order as $filed => $item) {
                $list = $list->orderBy($filed, $item);
            }
        }
        $list = $list->get();
        return $list;

    }

    public function getInAllList($whereIn = [], $field = ["*"], $order = ["id" => "desc"])
    {
        $list = $this->query()->select($field);
        if($whereIn){
            foreach ($whereIn as  $item) {
                $list = $list->whereIn(key($item), $item[key($item)]);
            }
        }
        if ($order) {
            foreach ($order as $filed => $item) {
                $list = $list->orderBy($filed, $item);
            }
        }
        $list = $list->get();
        return $list;

    }


    /**
     * 检测数据唯一
     * @param $where
     * @param int $id
     * @param string $pk
     * @return array
     */
    public function checkUnique($where, $id = 0, $pk = 'id')
    {
        try {

            if (empty($id)) {
                $has = $this->query()->select([$pk])->where($where)->first();
            } else {
                $has = $this->query()->select([$pk])->where($where)->where($pk, '<>', $id)->first();
            }
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '获取成功', $has ? $has->toArray() : []);
    }

    /**
     * 根据id获取信息
     * @param $where
     * @param string $field
     * @return array
     */
    public function findOne($where, $field = ['*'])
    {
        try {

            $info = $this->query()->select($field)->where($where)->first();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', !empty($info)?$info->toArray():[]);
    }

    /**
     * 添加单条数据
     * @param $param
     * @return array
     */
    public function insertOne($param)
    {
        try {

            $id = $this->query()->insertGetId($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '添加成功', $id);
    }

    /**
     * 根据id获取信息
     * @param $id
     * @param string $pk
     * @param string $field
     * @return array
     */
    public function getInfoById($id, $pk = 'id', $field = ['*'])
    {
        try {

            $info = $this->query()->select($field)->where($pk, $id)->first();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $info?$info->toArray():[]);
    }

    /**
     * 根据id更新数据
     * @param $param
     * @param $id
     * @param string $pk
     * @return array
     */
    public function updateById($param, $id, $pk = 'id')
    {
        try {

            $this->query()->where($pk, $id)->update($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '更新成功');
    }

    /**
     * 根据id删除
     * @param $id
     * @param string $pk
     * @return array
     */
    public function delById($id, $pk = 'id')
    {
        try {

            $this->query()->where($pk, $id)->delete();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '删除成功');
    }

    /**
     * 根据ids删除
     * @param $ids
     * @param string $pk
     * @return array
     */
    public function delByIds($ids, $pk = 'id')
    {
        try {

            $this->whereIn($pk, $ids)->delete();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '删除成功');
    }

    /**
     * 批量添加
     * @param $param
     * @return array
     */
    public function insertBatch($param)
    {
        try {
            $this->insert($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '添加成功');
    }

    /**
     * 根据ids更新
     * @param $param
     * @param $ids
     * @param string $pk
     * @return array
     */
    public function updateByIds($param, $ids, $pk = 'id')
    {
        try {

            if(!is_array($ids)){
                $ids=explode(',',$ids);
            }
            $this->whereIn($pk, $ids)->update($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '更新成功');
    }
    /**
     * 根据where条件更新数据
     * @param $param
     * @param $where
     * @return array
     */
    public function updateByWehere($param, $where)
    {
        try {

            $this->where($where)->update($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '更新成功');
    }

    /**
     * 根据$where条件删除
     * @param $where
     * @return array
     */
    public function delByWhere($where)
    {
        try {

            $this->where($where)->delete();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '删除成功');
    }

    /**
     * 获取指定条目的数据
     * @param array $where
     * @param int $limit
     * @param string $field
     * @param string $order
     * @return array
     */
    public function getLimitList($where = [], $limit = 10, $field = "*", $order = ["id"=> "desc"])
    {
        try {

            $list = $this->select($field)->where($where);
            if ($order) {
                foreach ($order as $filed => $item) {
                    $list = $list->orderBy($filed, $item);
                }
            }
            $list=$list->limit($limit)->get();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list?$list->toArray():[]);
    }

    /**
     * 获取带分页的列表
     * @param $limit
     * @param array $where
     * @param string $order
     * @param string $field
     * @return array
     */
    public function getPageList($limit, $where = [], $field = "*", $order = ['id'=>'desc'])
    {
        try {

            $list = $this->select($field)->where($where);
            if ($order) {
                foreach ($order as $filed => $item) {
                    $list = $list->orderBy($filed, $item);
                }
            }
            $list=$list->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list);
    }
}
