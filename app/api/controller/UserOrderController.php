<?php

namespace app\api\controller;

use app\api\service\OrderService;
use app\api\service\UserOrderService;

class userOrderController
{
    /**
     * 用户订单
     */
    public function index()
    {
        $param =request()->all();

        $orderService = new OrderService();
        $res = $orderService->getUserOrderList($param, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 订单信息
     */
    public function detail()
    {
        $orderId =request()->input('id');

        $orderService = new OrderService();
        $res = $orderService->getOrderDetail($orderId, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 取消订单
     */
    public function cancel()
    {
        $orderId =request()->input('id');

        $orderService = new UserOrderService();
        $res = $orderService->orderCancel($orderId, \Tinywan\Jwt\JwtToken::getCurrentId(),\Tinywan\Jwt\JwtToken::getUser()['name']);
        return json($res);
    }

    /**
     * 去支付
     */
    public function goPay()
    {
        $param = request()->post();
        $param['platform'] = isset(request()->header()['x-csrf-token']) ? request()->header()['x-csrf-token'] : '';

        $userOrderService = new UserOrderService();
        $res = $userOrderService->goPay($param, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 确认收货
     */
    public function received()
    {
        $orderId = input('param.id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->doReceived($orderId, $this->user['id'], $this->user['name']);
        if ($res['code'] == 0) {
            $res['msg'] = '操作成功';
        }

        return json($res);
    }

    /**
     * 评价
     */
    public function appraise()
    {
        if (request()->isPost()) {

            $userOrderService = new UserOrderService();
            return json($userOrderService->doAppraise(input('post.'), $this->user));
        }

        $orderId = input('param.order_id');
        $orderDetailId = input('param.order_detail_id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->getGoodsComments($orderId, $orderDetailId, $this->user['id']);
        return json($res);
    }

    /**
     * 物流信息
     */
    public function express()
    {
        $id = input('param.id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->getExpressInfo($id, $this->user['id']);
        return json($res);
    }

}
