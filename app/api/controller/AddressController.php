<?php

namespace app\api\controller;

use app\api\service\AddressService;
use app\api\service\CityService;

class AddressController
{

    /**
     * 添加地址
     */
    public function add()
    {

        $param = request()->post();

        $addressService = new AddressService();
        $res = $addressService->addUserAddress($param, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);

    }

    /**
     * 编辑地址
     */
    public function edit()
    {
        $param = request()->post();

        $addressService = new AddressService();
        $res = $addressService->editUserAddress($param, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 删除地址
     */
    public function del()
    {
        $id = request()->input('id');

        $addressService = new AddressService();
        $res = $addressService->delUserAddress($id, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 设置默认地址
     */
    public function setDefault()
    {
        $id = request()->input('id');

        $addressService = new AddressService();
        $res = $addressService->setDefault($id, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 获取默认的地址
     */
    public function getDefaultAddress()
    {
        $addressService = new AddressService();
        $res = $addressService->getDefaultAddress(\Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 获取用户地址
     */
    public function getUserAddress()
    {
        $addressService = new AddressService();
        $res = $addressService->getUserAddressList(\Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 省市区数据
     */
    public function area()
    {
        $cityService = new CityService();
        $res = $cityService->getAreaTree();
        return json($res);
    }
}