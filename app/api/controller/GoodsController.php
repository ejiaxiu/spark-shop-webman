<?php

namespace app\api\controller;

use app\api\service\GoodsService;

class GoodsController
{
    /**
     * 商品详情
     */
    public function detail()
    {
        $goodsId = request()->input('id');

        $goodsService = new GoodsService();
        $info = $goodsService->getMobileGoodsDetail($goodsId);
        return json($info);
    }

    /**
     * 商品规格详情
     */
    public function goodsRuleDetail()
    {
        $sku = request()->input('sku');
        $goodsId = request()->input('goods_id');

        $goodsService = new GoodsService();
        $res = $goodsService->getGoodsRuleDetail($sku, $goodsId);
        return json($res);
    }

    /**
     * 获取商品评论
     */
    public function getComments()
    {
        $goodsService = new GoodsService();
        $res = $goodsService->getComments(request()->all());
        return json($res);
    }

    /**
     * 获取分类下的商品
     */
    public function getGoodsByCateInfo()
    {
        $param =request()->all();

        $goodsService = new GoodsService();
        $res = $goodsService->getGoodsByCateId($param);
        return json($res);
    }
}
