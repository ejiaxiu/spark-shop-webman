<?php

namespace app\api\controller;

use app\api\service\LoginService;

class LoginController
{
    /**
     * 根据用户名登录
     */
    public function loginBySms()
    {
        $param = request()->post();

        $loginService = new LoginService();
        return json($loginService->doLoginBySms($param));
    }

    /**
     * 根据账号登录
     */
    public function loginByAccount()
    {
        $param = request()->post();

        $loginService = new LoginService();
        return json($loginService->doLoginByAccount($param));
    }

    /**
     * 注册
     */
    public function reg()
    {
        $param = request()->post();
        $loginService = new LoginService();
        $res = $loginService->userReg($param);
        return json($res);

    }

    /**
     * 根据微信登录
     */
    public function loginByWechat()
    {
        $code = request()->input('code');
        $loginCode = request()->input('login_code');
        $avatar = request()->input('avatar',   '/static/admin/default/image/avatar.png');

        $loginService = new LoginService();
        $res = $loginService->loginByWechat($code, $loginCode, $avatar);
        return json($res);
    }
}
