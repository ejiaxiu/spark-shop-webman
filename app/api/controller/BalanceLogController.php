<?php

namespace app\api\controller;

use app\api\service\BalanceLogService;

class BalanceLogController
{
    /**
     * 余额记录
     */
    public function index()
    {
        $param = request()->all();
        $param['user_id'] = \Tinywan\Jwt\JwtToken::getCurrentId();

        $balanceLogService = new BalanceLogService();
        return json($balanceLogService->getBalanceList($param));
    }

    /**
     * 获取基础信息
     */
    public function getTotalInfo()
    {
        $param = request()->all();
        $param['user_id'] = \Tinywan\Jwt\JwtToken::getCurrentId();
        $balanceLogService = new BalanceLogService();
        return json($balanceLogService->getTotalInfo($param));
    }
}