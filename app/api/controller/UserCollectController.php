<?php

namespace app\api\controller;

use app\api\service\UserCollectService;

class UserCollectController
{
    /**
     * 我的收藏
     */
    public function myCollect()
    {
        $param = request()->all();

        $userCollectService = new UserCollectService();
        return json($userCollectService->getMyCollect($param));
    }

    /**
     * 收藏
     */
    public function add()
    {
        $param = request()->all();

        $userCollectService = new UserCollectService();
        return json($userCollectService->addCollect($param));
    }

    /**
     * 移除收藏
     */
    public function remove()
    {
        $userCollectService = new UserCollectService();
        return json($userCollectService->removeCollect( request()->input('id')));
    }

    /**
     * 通过goods_id 移除收藏
     */
    public function removeByGoodsId()
    {
        $userCollectService = new UserCollectService();
        return json($userCollectService->removeCollectByGoodsId( request()->input('id')));
    }

}
