<?php

namespace app\api\controller;

use app\api\service\GoodsCateService;

class GoodsCateController
{
    /**
     * 商品分类列表
     */
    public function index()
    {
        $goodsCateService = new GoodsCateService();
        $res = $goodsCateService->getCateList();
        return json($res);
    }
}
