<?php

namespace app\api\controller;

use app\api\service\CartService;
use app\api\service\IndexService;
use app\api\service\OrderService;
use app\model\goods\GoodsCate;
use support\Request;

class IndexController
{
    /**
     * 小程序首页
     */
    public function index(Request $request)
    {
        $indexService = new IndexService();
        $data = $indexService->getIndexData();
        return json($data);
    }

    /**
     * 搜索
     */
    public function search(Request $request)
    {
        $indexService = new IndexService();
        $data = $indexService->search($request->post());
        return json($data);
    }


    /**
     * 获取营销插件的配置
     */
    public function marketingConfig()
    {
        $orderService = new OrderService();
        return json($orderService->getMarketingConfig());
    }

    /**
     * 获取购物车数量
     */
    public function cartNum()
    {
        $cartService = new CartService();
        $cartInfo = $cartService->getCartNum(\Tinywan\Jwt\JwtToken::getCurrentId());
        return json($cartInfo);
    }



}
