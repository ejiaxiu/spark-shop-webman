<?php

namespace app\api\controller;

use app\api\service\UserService;

class UserController
{

    /**
     * 获取用户中心首页数据
     */
    public function index()
    {
        $userService = new UserService();
        return json($userService->getMyBaseInfo(\Tinywan\Jwt\JwtToken::getCurrentId()));
    }

    /**
     * 获取用户基础数据
     */
    public function info()
    {
        $userService = new UserService();
        return json($userService->getUserInfo(\Tinywan\Jwt\JwtToken::getCurrentId()));
    }

    /**
     * 修改用户信息
     */
    public function update()
    {
        $param = request()->post();
        $userService = new UserService();
        return json($userService->updateInfo($param));
    }

    /**
     * 修改绑定手机号
     */
    public function changePhone()
    {
        $param = request()->post();
        $param['user_id'] = \Tinywan\Jwt\JwtToken::getCurrentId();

        $userService = new UserService();
        return json($userService->changePhone($param));
    }

    /**
     * 修改密码
     */
    public function changePassword()
    {
        $param =request()->post();
        $param['user_id'] = \Tinywan\Jwt\JwtToken::getCurrentId();

        $userService = new UserService();
        return json($userService->changePassword($param));
    }

}
