<?php

namespace app\api\controller;

use app\api\service\OrderRefundService;
use app\api\service\UserOrderService;

class OrderRefundController
{
    /**
     * 售后订单列表
     */
    public function index()
    {
        $param = request()->all();

        $orderRefundService = new OrderRefundService();
        $list = $orderRefundService->getRefundList($param);

        return json($list);
    }

    /**
     * 退款试算
     */
    public function refundTrail()
    {
        $param = request()->post();
        $param['user_id'] =  \Tinywan\Jwt\JwtToken::getCurrentId();

        $userOrderService = new UserOrderService();
        return json($userOrderService->refundTrail($param));
    }

    /**
     * 申请售后
     */
    public function refund()
    {
        $userOrderService = new UserOrderService();
        // 处理提交售后
        if (request()->method() === 'POST') {
            $param = request()->post();

            $res = $userOrderService->doRefundOrder($param, \Tinywan\Jwt\JwtToken::getUser());
            return json($res);
        }
    }

    /**
     * 订单详情
     */
    public function getDetail()
    {
        $refundId = request()->input('refund_id');

        $orderRefundService = new OrderRefundService();
        $info = $orderRefundService->getRefundDetail($refundId, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($info);
    }

    /**
     * 取消退款
     */
    public function cancelRefund()
    {
        $id = request()->input('id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->cancelRefund($id,\Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 快递信息查询
     */
    public function refundExpress()
    {
        $param = request()->post();

        $orderRefundService = new OrderRefundService();
        $res = $orderRefundService->doRefundExpress($param, \Tinywan\Jwt\JwtToken::getCurrentId());
        if ($res['code'] == 0) {
            $res['msg'] = '操作成功';
        }

        return json($res);
    }
}
