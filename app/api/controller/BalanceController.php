<?php

namespace app\api\controller;

use app\api\service\BalanceRechargeService;

class BalanceController
{
    /**
     * 余额充值
     */
    public function recharge()
    {
        $param = request()->post();
        $param['platform'] = isset(request()->header()['x-csrf-token']) ? request()->header()['x-csrf-token'] : '';

        $balanceRechargeService = new BalanceRechargeService();
        return json($balanceRechargeService->createOrder($param));
    }
}