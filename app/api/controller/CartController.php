<?php

namespace app\api\controller;

use app\api\service\CartService;

class CartController
{
    /**
     * 我的购车列表
     */
    public function list()
    {

        $limit = request()->input('limit');

        $cartService = new CartService();
        $list = $cartService->detail($limit, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($list);
    }

    /**
     * 获取购物车数量
     */
    public function num()
    {
        $cartService = new CartService();
        $cartInfo = $cartService->getCartNum(\Tinywan\Jwt\JwtToken::getCurrentId());
        return json($cartInfo);
    }

    /**
     * 添加购物车
     */
    public function add()
    {
        $param = request()->post();

        $cartService = new CartService();
        $res = $cartService->addCart($param, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 移除购物车
     */
    public function remove()
    {
        $id = request()->input('id');

        $cartService = new CartService();
        $res = $cartService->removeCartGoods($id, \Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

    /**
     * 清空购物车
     */
    public function clearCart()
    {
        $cartService = new CartService();
        $res = $cartService->clearCart(\Tinywan\Jwt\JwtToken::getCurrentId());
        return json($res);
    }

}