<?php

namespace app\api\service;

use app\model\goods\GoodsCate;

class GoodsCateService
{
    /**
     * 获取分类列表
     * @return array
     */
    public function getCateList()
    {
        $goodsCateModel = new GoodsCate();
        $list = $goodsCateModel->getAllList(['status' => 1], ['id','pid','name','icon','level'], ['sort'=> 'desc']);

        return dataReturn(0, 'success', $list);
    }
}
