<?php

namespace app\api\service;

use app\model\order\OrderDetail;
use app\model\order\OrderRefund;

class OrderRefundService
{
    /**
     * 售后订单列表
     * @param $param
     * @return array
     */
    public function getRefundList($param)
    {
        $userInfo =  \Tinywan\Jwt\JwtToken::getUser();
        $where[] = ['user_id', '=', $userInfo['id']];
        if (!empty($param['status'])) {
            $where[] = ['status', '=', $param['status']];
        }

        $orderRefundModel = new OrderRefund();
        $order = $orderRefundModel->getPageList($param['limit'], $where);

        $refundStatus = config('order.refund_status');
        $orderDetailModel = new OrderDetail();
        $list=returnPage($order['data']);
        foreach ($list['data'] as $k=>$i){
            $data = json_decode($i['apply_refund_data'], true);
            $detailIds = [];
            foreach ($data['order_num_data'] as $vo) {
                $detailIds[] = $vo['order_detail_id'];
            }
            $listWhere[] = ['id', 'in', $detailIds];
            $orderDetailList = $orderDetailModel->getInAllList($listWhere)->toArray();
            $list['data'][$k]['goodsList'] = $orderDetailList;
            if ($i['refund_type'] == 2 && $i['step'] == 3) {
                $list['data'][$k]['status_txt'] = '待退货';
            } else {
                $list['data'][$k]['status_txt']= $refundStatus[$i['status']];
            }
        }

        return dataReturn(0, 'success', $list);
    }

    /**
     * 售后订单详情
     * @param $refundId
     * @param $userId
     * @return array
     */
    public function getRefundDetail($refundId, $userId)
    {
        // 退款申请详情
        $orderRefundModel = new OrderRefund();
        $info = $orderRefundModel->with(['orderInfo.address'])->where('id', $refundId)->where('user_id', $userId)->first();

        $applyDetailData = json_decode($info['apply_refund_data'], true);
        $detailIds = [];
        $id2Num = [];
        foreach ($applyDetailData['order_num_data'] as $vo) {
            $detailIds[] = $vo['order_detail_id'];
            $id2Num[$vo['order_detail_id']] = $vo['num'];
        }

        $payWay = config('order.pay_way');
        $orderStatus = config('order.pay_status');

        $info['orderInfo']['pay_status'] = $orderStatus[$info['orderInfo']['pay_status']];
        $info['orderInfo']['pay_way'] = $payWay[$info['orderInfo']['pay_way']];

        // 订单详情
        $totalAmount = 0;
        $orderDetailModel = new OrderDetail();
        $detailList = $orderDetailModel->whereIn('id', $detailIds)->get();
        foreach ($detailList as $key => $vo) {
            $detailList[$key]['apply_refund_num'] = $id2Num[$vo['id']];
            $totalAmount = round(($totalAmount + $vo['price'] * $id2Num[$vo['id']]), 2);
        }

        $info['order_detail'] = $detailList;
        $info['order_price'] = $totalAmount;

        // 退货地址
        $info['refund_address'] = getConfByType('shop_refund');

        return dataReturn(0, 'success', $info);
    }

    /**
     * 快递信息查询
     * @param $param
     * @param $userId
     * @return array
     */
    public function doRefundExpress($param, $userId)
    {
        if (empty($param['refund_express_name']) || empty($param['refund_express'])) {
            return dataReturn(-2, '快递名或快递单号不能为空');
        }

        $refundModel = new OrderRefund();
        $refundInfo = $refundModel->findOne([
            'id' => $param['id'],
            'user_id' => $userId
        ])['data'];

        if (empty($refundInfo)) {
            return dataReturn(-1, '订单信息异常');
        }

        $param['step'] = $refundInfo['step'] + 1;
        return $refundModel->updateById($param, $param['id']);
    }
}
