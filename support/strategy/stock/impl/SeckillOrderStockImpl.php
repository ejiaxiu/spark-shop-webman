<?php

namespace support\strategy\stock\impl;

use support\strategy\stock\StockInterface;
use Webman\Event\Event;
class SeckillOrderStockImpl implements StockInterface
{
    public function dealStockAndSales($param)
    {
      return  Event::emit('seckillStock', $param)[0];

    }
}