<?php

namespace support\strategy\stock;

interface StockInterface
{
    public function dealStockAndSales($param);
}